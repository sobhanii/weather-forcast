public interface WeatherAPI {
    WeatherData getWeatherData();

    double getTemperature();

    double getHumidity();

    double getPressure();
}
