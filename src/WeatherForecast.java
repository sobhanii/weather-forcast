public class WeatherForecast {
    private WeatherForecastStrategy weatherForecastStrategy;

    public void setWeatherForecastStrategy(WeatherForecastStrategy weatherForecastStrategy) {
        this.weatherForecastStrategy = weatherForecastStrategy;
    }

    public String generateWeatherForecast(WeatherData weatherData) {
        return weatherForecastStrategy.generateWeatherForecast(weatherData);
    }
}
