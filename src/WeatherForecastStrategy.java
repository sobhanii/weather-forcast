public interface WeatherForecastStrategy {
    String generateWeatherForecast(WeatherData weatherData);
}
