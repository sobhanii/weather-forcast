public class OpenWeatherMapAPI implements WeatherAPI{
    @Override
    public WeatherData getWeatherData() {
        // Implement code to call OpenWeatherMap API and get weather data
        return new WeatherData(78.6f, 60f, 29.2f);
    }
}
