import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class WeatherAPIAdapter implements WeatherAPI{
    private WeatherAPI weatherAPI;
    private List<WeatherObserver> observers;

    public WeatherAPIAdapter(WeatherAPI weatherAPI) {
        this.weatherAPI = weatherAPI;
        this.observers = new ArrayList<WeatherObserver>();
    }
    

    public WeatherData getWeatherData() {
        double temperature = weatherAPI.getTemperature();
        double humidity = weatherAPI.getHumidity();
        double pressure = weatherAPI.getPressure();
        return new WeatherData((float) temperature, (float) humidity, (float) pressure);
    }

    @Override
    public double getTemperature() {
        return 2.5;
    }

    @Override
    public double getHumidity() {
        return 3.6;
    }

    @Override
    public double getPressure() {
        return 4.9;
    }

    public void registerObserver(CurrentConditionsDisplay currentConditionsDisplay) {
    }
}
