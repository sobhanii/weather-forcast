public class MachineLearningStrategy implements WeatherForecastStrategy{
    @Override
    public String generateWeatherForecast(WeatherData weatherData) {
        // Use machine learning algorithms to generate a forecast
        return "Partly cloudy";
    }
}
