public interface WeatherObserver {
    void update(WeatherData weatherData);
}
