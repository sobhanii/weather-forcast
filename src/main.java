public class main {
    public static void main(String[] args) {
        // Create a weather station and register a current conditions display
        WeatherStation weatherStation = new WeatherStation();
        CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay();
        weatherStation.registerObserver(currentConditionsDisplay);

        // Set the weather data and notify the observers
        WeatherData weatherData = new WeatherData(72.5f, 55f, 30f);
        weatherStation.setWeatherData(weatherData);

        // Change the forecast strategy to historical data analysis and generate a forecast
        HistoricalDataAnalysisStrategy historicalDataAnalysisStrategy = new HistoricalDataAnalysisStrategy();
        WeatherForecast weatherForecast = new WeatherForecast();
        weatherForecast.setWeatherForecastStrategy(historicalDataAnalysisStrategy);
        String forecast = weatherForecast.generateWeatherForecast(weatherData);
        System.out.println("Weather forecast: " + forecast);

        // Use the OpenWeatherMap API and an adapter to get weather data and notify the observers
        OpenWeatherMapAPI openWeatherMapAPI = new OpenWeatherMapAPI();
        WeatherAPIAdapter weatherAPIAdapter = new WeatherAPIAdapter(openWeatherMapAPI);
        weatherAPIAdapter.registerObserver(currentConditionsDisplay);
        WeatherData apiWeatherData = weatherAPIAdapter.getWeatherData();
        weatherStation.setWeatherData(apiWeatherData);
    }
}
